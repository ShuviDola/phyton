from math import pi
# Cambiando cosas

class Coordenada:
    def __init__(self, x, y, z):
        self.__x = x
        self.__y = y
        self.__z = z
        
    def setX (self,x):
        self.__x = x

    def setY (self,y):
        self.__y = y

    def setZ (self,z):
        self.__z = z

    def getX(self):
        return self.__x

    def getY(self):
        return self.__y

    def getZ(self):
        return self.__z

    def toString(self):
        return "(" + str (self.__x) + ", " + str(self.__y) + ", " + str(self.__z) + ")"




class Cuadrado:

    def __init__(self, pAltura, coordenada):
        self.setAltura(pAltura)
        self.setCoordenada(coordenada)


    def calculoCuadrado(self):
        return "La superficie del cuadrado es: " + str(self.__altura * self.__altura)

    def setAltura (self, pAltura):
        if type(pAltura) is float or type(pAltura) is int:
            if pAltura <= 0:
                print("Ingrese un número positivo")
            else:
                self.__altura = pAltura
        else:
            print("Ingrese un número")

    def getAltura(self):
        return "La altura del cuadrado es: " + str(self.__altura)


    def setCoordenada (self, coordenada):
        self.__coordenada = coordenada

    def getCoordenada (self):
        return self.__coordenada






class Triangulo:

    def __init__(self,pAltura,pBase,coordenada):
        self.setAltura(pAltura)
        self.setBase(pBase)
        self.setCoordenada(coordenada)
    
    def CalculoTriangulo(self):
        return "La superficie del triangulo es: " +str(self.__altura *self.__base/2)

    def setAltura (self, pAltura):
        if type(pAltura) is float or type(pAltura) is int:
            if(pAltura) <=0:
                print ("Ingrese un número positivo")
            else:
                self.__altura =pAltura
        else:
            print("Ingrese un número")

    def getAltura(self):
        return ("La altura del triangulo es: " + str(self.__altura))
    

    def setBase (self, pBase):
        if type(pBase) is float or type(pBase) is int:
            if(pBase) <=0:
                print ("Ingrese un número positivo")
            else:
                self.__base=pBase
        else:
            print("Ingrese un número")

    def getBase(self):
        return ("La base del triangulo es: " + str(self.__base))


    def setCoordenada (self, coordenada):
        self.__coordenada = coordenada

    def getCoordenada (self):
        return self.__coordenada




class Circulo: 
        
    def calculoCirculo (self):
        return (pi*(self.__radio*self.__radio))

    def setRadio(self, pRadio):
        if type(pRadio) is float or type(pRadio) is int:
            if pRadio <=0:
                print("Escribe un número positivo")
            else:
                self.__radio =pRadio
        else:
            print("Escribe un número")

    def getRadio(self):
        return ("El radio del circulo es: " +str(self.__radio))

    def __init__(self, pRadio, coordenada):
        self.setRadio(pRadio)
        self.setCoordenada(coordenada)
        
    def setCoordenada (self, coordenada):
        self.__coordenada = coordenada

    def getCoordenada (self):
        return self.__coordenada
    
    def toString (self):
        return self.__coordenada.toString()

    def __init__(self, pRadio, coordenada):
        self.setRadio(pRadio)
        self.setCoordenada(coordenada)

class Rectangulo: 
        
    def calculoRectangulo (self):
        return (self.__base*self.__altura)

    def setBase(self, pBase):
        if type(pBase) is float or type(pBase) is int:
            if pBase <=0:
                print("Escribe un número positivo")
            else:
                self.__base =pBase
        else:
            print("Escribe un número")

    def getBase(self):
        return ("La base del rectangulo es: " +str(self.__base))
    
    def setAltura(self, rAltura):
        if type(rAltura) is float or type(rAltura) is int:
            if rAltura <=0:
                print("Escribe un número positivo")
            else:
                self.__altura =rAltura
        else:
            print("Escribe un número")

    def getAltura(self):
        return ("La altura del circulo es: " +str(self.__altura))
        
    def setCoordenada (self, coordenada):
        self.__coordenada = coordenada

    def getCoordenada (self):
        return self.__coordenada
    
    def toString (self):
        return self.__coordenada.toString()

    def __init__(self, rBase, rAltura, coordenada):
        self.setBase(rBase)
        self.setAltura(rAltura)
        self.setCoordenada(coordenada)


class Rombo:

    def __init__(self,pDiagonalMayor,pDiagonalMenor,coordenada):
        self.setDiagonalMayor(pDiagonalMayor)
        self.setDiagonalMenor(pDiagonalMenor)
        self.setCoordenada(coordenada)
    
    def CalculoRombo(self):
        return "La superficie del rombo es: " +str(self.__diagonal_mayor *self.__diagonal_menor/2)

    def setDiagonalMayor (self, pDiagonalMayor):
        if type(pDiagonalMayor) is float or type(pDiagonalMayor) is int:
            if(pDiagonalMayor) <=0:
                print ("Ingrese un número positivo")
            else:
                self.__diagonal_mayor =pDiagonalMayor
        else:
            print("Ingrese un número")

    def getDiagonalMayor(self):
        return ("La Diagonal mayor del rombo es: " + str(self.__diagonal_mayor))
    

    def setDiagonalMenor (self, pDiagonalMenor):
        if type(pDiagonalMenor) is float or type(pDiagonalMenor) is int:
            if(pDiagonalMenor) <=0:
                print ("Ingrese un número positivo")
            else:
                self.__diagonal_menor=pDiagonalMenor
        else:
            print("Ingrese un número")

    def getDiagonalMenor(self):
        return ("La Diagonal Menor del rombo es: " + str(self.__diagonal_menor))


    def setCoordenada (self, coordenada):
        self.__coordenada = coordenada

    def getCoordenada (self):
        return self.__coordenada

    

rombo1 = Rombo(10,5,Coordenada(2,4))

print(rombo1.getDiagonalMayor())
print(rombo1.CalculoRombo())

    
